 (*   Importing libraries - public version for confirmation  *)
require import AllCore Int DInterval List FSet SmtMap Bool.
require (*--*) Ring Bigop ZModP.


op p: { int | 2 <= p } as le2_p.

clone import ZModP as PF with
  op p <- p
proof le2_p by exact/le2_p.
import ZModpRing.

(* G1 *)
theory Group1.       (* a random generator gen1 ∈ G1(Group1) *)
type g1.              (* elements in G1  *) 
op oneg1: g1.
op ( * ): g1 -> g1 -> g1.
op inv: g1 -> g1.

clone include Ring.ZModule with
  type t        <- g1,
  op   zeror    <- oneg1,
  op   (+)      <- ( * ),
  op   [-]      <- inv   (* negation of multiplication eg. (1/4) inv 4 *)

rename "intmul" as "intpow"
rename "addr"   as "mulg1"
rename "add0r"  as "mul1g1"  (* for Bigop  *)
rename "add2r"  as "mul2g1"
rename "mulr"   as "powg1"
rename "subr"   as "invg1".

(* cloning Bigop /(BIGALG) to get multiplication in Prove Generic protocol *)
clone include Bigop with
  type t           <- g1,
  op   Support.idm <- oneg1,
  op   Support.(+) <- ( * )
proof Support.Axioms.*.
realize Support.Axioms.addmA by exact/mulg1A.
realize Support.Axioms.addmC by exact/mulg1C.
realize Support.Axioms.add0m by exact/mul1g1.
    
op (^) (g : g1) (x : zmod) = intpow g (asint x). 
(* need a operator that takes a g1,into zmod. so instead of taking an integer now, we take the zmod and it is intpow as int  x (it is an lement in zp.) zmod= o..p-1   *)

(* a generator *)
op g1: g1.
op gen1: g1.
end Group1.
import Group1.

(* G2 *)
theory Group2.
type g2.
op oneg2: g2.
op ( * ): g2 -> g2 -> g2.
op inv: g2 -> g2.

clone include Ring.ZModule with
  type t     <- g2,
  op   zeror <- oneg2,
  op   (+)   <- ( * ),
  op   [-]   <- inv

rename "intmul" as "intpow"
rename "addr"   as "mulg2"
rename "add2r"  as "mul2g2"
rename "mulr"   as "powg2"
rename "subr"   as "invg2".

op (^) (g : g2) (x : zmod) = intpow g (asint x).
 
(* a generator *)
op g2: g2.
op gen2: g2.

end Group2.
import Group2.

(* GT *)
theory Groupt.
type gt.
op onegt: gt.
op ( * ): gt -> gt -> gt.
op inv: gt -> gt.

clone include Ring.ZModule with
  type t     <- gt,
  op   zeror <- onegt,
  op   (+)   <- ( * ),
  op   [-]   <- inv

rename "intmul" as "intpow"
rename "addr"   as "mulgt"
rename "add2r"  as "mul2gt"
rename "mulr"   as "powgt"
rename "subr"   as "invgt".

op (^) (g : gt) (x : zmod) = intpow g (asint x).
end Groupt.
import Groupt.

op e: g1 -> g2 -> gt.
axiom e_bilinear (x y : zmod): e (g1^x) (g2^y) = (e g1 g2)^(x * y).
  (* (g1, g1^x, g1^x^2, .., g1^x^q)  ∈ G1 * Zp  *)


  (*  1).    Assumption q_SDH   *)
module type QSDH_Adv ={
  proc main(g1s: g1 list, g2: g2, g2x: g2): zmod * g1
}.
op qpow (x : zmod) (q : int) =
  map (fun (i : int)=> g1^(exp x i)) (range 0 (q+1)).

op dp = Distr.dapply inzmod [1..p-1].

module Test1 (A: QSDH_Adv) = {
  proc test(q:int) = {
    var x, c, h;
   
    x     <$ dp;
    (c,h) <@ A.main(qpow x q, g2, g2^x);   
    return h = g1 ^ (inv (x + c));
  }
}.

    (*  ___________________________________________________
        2)  Assumption2 (LRSW)  *)

 (* LRSW_Adv:  (a,b,c,m) <- A^(o (x, y) (.)) (X,Y) *)
module type Oracle = {
   proc o(m:zmod) : g1 * g1 * g1
   }. 
 
module type Adv (O:Oracle)= {
   proc main (x y: g2): g1 * g1 * g1 * zmod
 }. 
 
(* print dp.
 op dp1 = Distr.dapply inzmod [0..p-1]. *)
     (* let 0 X,Y(.) be an oracle that, on input a value m∈Zp, outputs a triple (a, a^y, a^x+xym) for a randomly chosen a.
defined the adversay locally instead of globally
 *)
 op dg1 : g1 distr.
 
 module LRSW(A:Adv) ={
  var x : zmod
  var y : zmod
  var q : zmod fset

  module O = {
   proc o(m:zmod) ={
     var a1, a2, a3;
     q  <- q `|` fset1 m;
     a1 <$ dg1;
     a2 <- a1 ^ y; 
     a3 <- a1 ^ (x + x * y * m);
     return (a1,a2,a3); 
   }
 } 
  proc main(): bool ={
    var xX, yY, a,b,c,m;
   (* (x, y) <-$ Zp^2  (samples x and y)  *)
   x <$ dp;      
   y <$ dp;
   q <- fset0;     
 (* Q is a set of queries made to 0X,Y(.) and has an empty set intially *)

  xX  <- g2 ^ x;   (* X = g2^x *)
  yY  <- g2 ^ y;    (* Y = g2^y *)
 (a, b, c, m) <@ A(O).main(xX, yY);

   (* m !∈ Q => !m \in q *)
    return !m \in q /\ a <> oneg1 /\ b= a^y /\ c= a ^ (x + x*y*m);
    }
  }.

  (* _______________________________________
   
       3) Assumptions (Generalized LRSW) 

      *)
  print fset1.
  print ( `|` ).
  search ( `|` ).
  module type Oracle1 ={
    proc o1(): g1 * g1
    proc o2(a b: g1, m: zmod): g1 option
  }.

  module type Adv1 (O:Oracle1)= {
    proc main (x y: g2): g1 * g1 * g1 * zmod
  }.

  module G_LRSW(A:Adv1) ={
  var x : zmod
  var y : zmod
  var q : zmod fset
  var l : (g1 * g1)  fset (*a pair of group1 elements *)
    
  module O1 = {
   proc o1(): g1 * g1 ={
     var a, b;
     a <$ dg1;
     b <- a ^ y;
     l <- l `|` fset1 (a,b);  (* list Union finite set one has a pair of G1 elements *)
     return (a, b); 
    }

     proc o2(a b: g1, m:zmod): g1 option ={
        var c;
        c <- None;
        q <- q `|` fset1 m;
      
        if( (a, b) \in l) {
        c <- Some (a^ (x+x*y*m));
        }

         return c;      
      }
    } (* end O *)
      
    proc main(): bool ={
      var xX, yY, a,b,c,m;
      x <$ dp;      
      y <$ dp;
      q <- fset0;     

      xX  <- g2 ^ x;   (* X = g2^x *)
      yY  <- g2 ^ y;    (* Y = g2^y *)
     (a, b, c, m) <@ A(O1).main(xX, yY);

   (* m !∈ Q => !m \in q *)
    return !m \in q /\ a <> oneg1 /\ b= a^y /\ c= a ^ (x + x*y*m);
    }
  }.


   (* _____________Oracles_____________
   ***** TPM Interfaces***             *)
require Lazy BitWord.

clone Lazy as H with
  type from    <- bool list, (* bool list should be replaced with finite bitstring *)
  type to      <- zmod,                                                                 
  op dsample _ <- dp.

clone Lazy as Hg1 with
  type from    <- bool list, (* bool list should be replaced with finite bitstring *)
  type to      <- g1,
  op dsample _ <- dg1.

op ln: { int | 0 < ln } as gt0_ln.
op zmod2bs: zmod -> bool list. (* one-to-one? *)

clone import BitWord as Nonce with
  op n <- ln
proof gt0_n by exact/gt0_ln.

op tpm_prefix: bool list.
op nonce_prefix: bool list.
op fs_prefix: bool list. print H.ARO. print H.Oracle.


module type TPM(H:H.ARO, H1: Hg1.ARO) = {
  proc create(): unit
  proc hash(mt: bool list, mh: bool list): zmod
  proc commit(bsnE: bool list, bsnL: bool list): int * zmod * g1 * g1 option * g1 option
  proc sign(cid: int, c: zmod, nh: Nonce.word): (Nonce.word * zmod) option
}.
  
module TPM(H: H.ARO, H1: Hg1.ARO) = {  (* instead of calling H.RO.o, using H.ARO to call it without init() from Oracle  *)
  var created: bool (*if this is true, do nothing *)
  var tpk: g1
  var tsk: zmod
  var sts: zmod fset
  var commitId: int
  var committed: (int, zmod * Nonce.word) fmap
  
  proc create(): unit = {  (* Init *)
    if (!created) {
      created <- true;
      tsk <$ dp;
      tpk <- g1^tsk;
      commitId <- 0;
      committed <- empty;
    }
  }

  proc hash(mt: bool list, mh: bool list): zmod = {
    var c;
    if (size mt <> 0) {
      (* the TPM checks whether it wants to attest to m_t *)
    }
    c   <- H.o(tpm_prefix ++ mt ++ mh);
    sts <- sts `|` fset1 c;
    return c;
  }

  proc commit(bsnE: bool list, bsnL: bool list): int * zmod * g1 * g1 option * g1 option = {
    var gt, r, nt, ntb, e, k, l, j, c;
    
    if (size bsnE <> 0) {
      gt <@ H1.o(bsnE);
    } else {
      gt <- g1;
    }
        r  <$ dp;
    (*  *)
    nt <$ Nonce.DWord.dunifin;
    committed.[commitId] <- (r,nt);
    ntb <@ H.o(nonce_prefix ++ Nonce.ofword nt);
    e <- gt^r;
    k <- None;
    l <- None;
    if (size bsnL <> 0) {
      j <@ H1.o(bsnL);
      k <- Some (j^tsk); (* takes a group elements and gives us a type which is either group or error symbol (None). *)
      l <- Some (j^r);
    }
    c <- commitId;
    commitId <- commitId + 1;
    return (c,ntb,e,k,l);
  }

  proc sign(cid: int, c: zmod, nh: Nonce.word): (Nonce.word * zmod) option = {
    var rnt, r, nt, ret, c', s;     (* c': prime *)

    ret <- None; 
    rnt <- committed.[cid];
    committed <- rem committed cid;
    if (rnt <> None) {
      (r,nt) <- oget rnt;
      if (c \in sts) {
        c' <@ H.o(fs_prefix ++ (ofword (nt +^ nh)) ++ (zmod2bs c));  
        s <- r + c' * tsk;
        ret <- Some (nt,s);  
      }
    }
    return ret;
  }
}.


    (* ________ Generic Prove Protocol __________ *)

clone Lazy as H_ with
  type from    <- bool list, (* bool list should be replaced with finite bitstring *)
  type to      <- zmod,
  op dsample _ <- dp.

clone import BitWord as Nonce_ with
  op n <- ln
proof gt0_n by exact/gt0_ln.

op nonce_prefix_: bool list.
op fs_prefix_: bool list.

op dgt: gt distr.

(* Created the injective (the image) for the mh' argument (just before call TMP.Has(mt, mh')) *)
 op format: bool list -> g1 -> g1 -> (g1 * g1 * g1) list -> g1 -> bool list -> g1 option -> g1 option -> g1 option -> bool list.
  axiom format_inj mh y1 ghd bs t1 bsnL t2 y3 t3 mh' y1' ghd' bs' t1' bsnL' t2' y3' t3':
    format mh y1 ghd bs t1 bsnL t2 y3 t3 = format mh' y1' ghd' bs' t1' bsnL' t2' y3' t3' =>
    mh = mh' /\
    y1 = y1' /\
    ghd = ghd' /\
    bs = bs' /\
    t1 = t1' /\
    bsnL = bsnL' /\
    t2 = t2' /\
    y3 = y3' /\
    t3 = t3'.

  module type Prove (T: TPM, H:H.ARO, H1:Hg1.ARO) ={
     proc test(hsk: zmod, 
           y1 : g1,        bsnE:bool list,
           _delta:zmod,    gt2: g2 option, 
           gamma: zmod,    bsnL: bool list, 
           y3: g1 option,  list_i: (zmod * g1 * g1 * g1) list, 
           mh: bool list,   mt: bool list)
      : (g1 * (zmod * Nonce.word * zmod * zmod list)) option
   (*  {T.commit T.hash T.sign } (*restricting access to TPM not to call create as TPM was calling before *) *)
    proc kg() : g1 * zmod (*pkey * skey, we wrote the types of these two *)      
  }.

  module Prove(T: TPM, H:H.ARO, H1:Hg1.ARO)  = {
  proc test(hsk: zmod, 
           y1 : g1,        bsnE:bool list,
           _delta:zmod,    gt2: g2 option, 
           gamma: zmod,    bsnL: bool list, 
           y3: g1 option,  list_i: (zmod * g1 * g1 * g1) list, 
           mh: bool list,   mt: bool list)
      : (g1 * (zmod * Nonce.word * zmod * zmod list)) option  = {

    var g_tilda, g_hat, y2, _J,commitId,  nBar_t, _E, _K, _L, rhsk, e', k', l', r, rs, t1, t2, t3, mh', c, nh, s, s', c', s_alpha,  sa, _j, _q, n, pi, nBar_t', ret;
    var nt: Nonce.word;

    ret <- None;  (* this return is used for the Check nBar_t statement, because we using nested if statements for the (i):'check=nbar_t'  and (ii): 'check= (g_hat^_delta)s'.
from nBar_t to the end, we used it in one bracket.  *)
       
(* bsnE  *)
    if (size bsnE <> 0) {
      g_tilda <@ Hg1.RO.o(bsnE);
    }else {
      g_tilda <- g1;
    }
(* gt2 ...
 note g_hat is in Gt, gt2 is in G2, g_tilda in G1  
 assumed gt2 is bottom (said it in the paper) 
  
if ( gt2 <> None) {
     ---->  g_hat <- e g_tilda (oget gt2);  (* e(gt, gt2) *) This gives me an error because its in the target Group but expected G1 . 

     g_hat <$  dg1;  (* or?  g_hat <$ dgt;  *) If I sample it from G1 then it is not correct. Again not right for the security proof.
      }else { *)
  
    g_hat <- g_tilda;  (* SOL: without calling gt2, I can simply call that g_hat is g_tilda because in the full version of paper, it said that assumed gt2 is bottom  *)
  
(* bsnL != bottom *)  
    if (size bsnL <> 0) {
      _J <@Hg1.RO.o(bsnL);
    }  

(* call TPM.Commit  *)       
    (commitId, nBar_t, _E, _K, _L) <@ T(H,H1).commit(bsnE, bsnL);  


(* rhsk <-$ Zmodp  *)
    rhsk <$ dp;         (* rhsk <-$ zmod  *)
    e' <- (_E * g_tilda^rhsk)^(gamma * _delta); 
    (* if size of bsnL is different then 0, then _K & _L have a real value. oget gives us the real value *)       
    if (size bsnL <> 0) {
      k' <- ((oget _K)* _J^hsk)^gamma;
      l' <- ((oget _L)* _J^hsk)^gamma; 
    }
   
(* bsnL != bottom  *)
    if (size bsnL <> 0) {
      y2 <-  k' *  (big predT (fun (abbb : _ * _ * g1 * _) => 
        abbb.`3^abbb.`1) list_i); 
 (*OR we can say that abbb(name): zmod * g1 * g1 *g1 *)
 (*FROM BIGOP FILE:  op big (P : 'a -> bool) (F : 'a -> t) (r : 'a list) =
  foldr Support.(+) idm (map F (filter P r)).   *)
    }

 (* Take r Alpha_i *)
    rs <- [];
    while (size rs < size list_i) {
      r  <$ dp;
      rs <- r::rs;
    }
 
   t1 <- e' * (bigi predT (fun j=> (nth witness list_i j).`2 ^ 
        (nth witness rs j)) 0 (size list_i - 1)); 

    if (size bsnL <> 0){
      t2 <- Some (l' * (bigi predT (fun j=> (nth witness list_i j).`3 ^ 
            (nth witness rs j)) 0 (size list_i - 1)));
    } else {
      t2 <- None;
    }
 
    if (y3 <> None) {
      t3 <- Some (bigi predT (fun j=> (nth witness list_i j).`4 ^ 
          (nth witness rs j)) 0 (size list_i - 1));
    } else {
      t3 <- None;
    } 

(* Set  mh' *)
    mh' <- format mh y1 (g_tilda^_delta) (map (fun (abbb : _ * _ * _ * _)=> (abbb.`2,abbb.`3,abbb.`4)) list_i) t1 bsnL t2 y3 t3;

(* Call TPM.Hash  *)
    c <@ T(H, H1).hash(mt,mh');
    nh <$ Nonce.DWord.dunifin;

(* Call TPM.Sign  *) 
(*  I: we know TPM.Sign is succesfull because commitID 
    II:  *)
    _q <@ T(H, H1).sign(commitId, c, nh);
    (nt, s) <- oget (_q);  
(* II 
    _q <@ T.sign(commitId, c, nh);
     if (_q <> None) {
      (nt, s) <- oget _q;
     }   *)       

(* Check ntBar = H *)
    nBar_t' <@ H.o(nonce_prefix_ ++ (Nonce.ofword nt));
    if (nBar_t = nBar_t') {
      n      <- nh +^ nt;
      c'     <@ H.o(fs_prefix_ ++ (ofword n) ++ (zmod2bs c));
(* Set s' <- gamma  *)
      s'      <- gamma * (s + rhsk + (c' * hsk));
(* (nth witness list_i _j) is the jth of  alpha_j, b_j, b'_j,.
 then we pick the 1st element & multiply it with c'.
(list_i. `1). `1  (rs= {r_alpha_1, ...,r_alpha_i} <- r_alpha_j)  *)        
      s_alpha <- [];
      _j      <-  0;
      while (_j < size list_i) {
        sa     <- (nth witness rs _j) + (c' * (nth witness list_i _j).`1);
        s_alpha <- sa::s_alpha;
        _j     <- _j + 1;
      } 
 
(* Check (g_hat ^_delta)^s' = E'...  *)  
      if((g_hat^_delta)^s' <>  e' * (y1 * (inv  (big predT (fun (abibb : _ * g1 * _ * _) => 
          abibb.`2^abibb.`1) list_i)))^c') {
        if (size bsnE = 0 \/ _J^s' = l'*(k'^c')) {
          pi <- (c', n, s', s_alpha);
          ret <- Some (y2,pi);
        }
      }
    }
    return ret;
  }
}.


(* ----------------- VerSKP --------------- *)
search None oget. (* lemma some_oget  *)
module VerSPK = {
   proc veri(pi:(zmod  * word * zmod * zmod list),
             y1:g1,            
             g_hat_delta:g1,
             y2:g1,
             bsnL:bool list, 
             y3:g1 option,
             list_i:(zmod * g1* g1 * g1) list, 
             mh:bool list, 
             mt: bool list  )
          : bool = {
   var c', n, s', s_alpha, t1, hh, t2, t3, hash_r2, hash_r1;
  
   (*PI knows that s_alpha is a list bcoz we hv defined it that veri takes a zmod list (the end) *)
   (c', n, s', s_alpha) <- pi;
   
   t1  <-  y1^ (-c') * (g_hat_delta)^s'  *
           (bigi predT (fun j=> (nth witness list_i j).`2 ^
                              (nth witness s_alpha j))
                                0 (size list_i - 1)); (* size is from 0 to l-1  *)
   t2  <-  None;
   if (size bsnL <> 0) {
       hh <@ Hg1.RO.o(bsnL); (* abstract random oracle which models the hash func *)
   t2  <- Some (y2^(-c') * (* Hg1.RO.o(bsnL) *) hh^s' * 
          (bigi predT (fun j=> (nth witness list_i j).`3 
            ^(nth witness s_alpha j))
               0 (size list_i - 1)) ); 
     }(*  else {
      t2 <-  None;
       }*)   
    t3  <- None; 

    if (y3 <> None) {
        t3 <- Some (( oget y3)^(-c') * (* used oget bcoz y3 has been used before (when it was not bottom/ None)  *)
              (bigi predT (fun j=> (nth witness list_i j).`4 
                  ^(nth witness s_alpha j)) 0 (size list_i - 1)) );
      } (* else {
          t3 <- None;
          } *) 
     hash_r2 <@ H.RO.o(tpm_prefix ++ mt ++
                  format mh y1 (g_hat_delta) (map (fun (abbb : _ * _ * _ * _)=>
                     (abbb.`2,abbb.`3,abbb.`4)) list_i) 
                                  t1 bsnL t2 y3 t3); 

     hash_r1 <@ H.RO.o(fs_prefix ++ (ofword n) ++ (zmod2bs hash_r2)); 
    
     return c' =  hash_r1; (* if c'= .. {output true/1} else {false/0} *)
    }
}.


